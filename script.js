// console.log("Hello");

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 11);
let mewtwo = new Pokemon("Mewtwo", 12);
let bulbasaur = new Pokemon("Bulbasaur", 11);


ashePokemons=[pikachu,geodude,mewtwo,bulbasaur]
// console.log(friends[0].pokemonName);

const Trainer={
    name: "Ash Ketchum",
    age: 12,
    friends:{
        hoenn:['May','Max'],
        kanto:['Brock','Misty']
    },
    pokemon:ashePokemons,
    talk:function(pokemonUse){
        console.log(pokemonUse.pokemonName+"! i choose you!");
    }    
}

// function Trainer(name,age,pokemons){
//     this.name=name;
//     this.age=age;
//     this.friends=pokemons;
//     this.talk=function(pokemonUse){
//         console.log(pokemonUse.pokemonName+"! i choose you!");
//     }    


// }
// let friends=[pikachu,geodude,mewtwo,bulbasaur];

console.info("Result of dot notation:");
Trainer.talk(pikachu);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);
console.log(bulbasaur);


function Pokemon(name,level){
    // Properties Pokemon
    this.pokemonName = name;
    this.pokemonLevel= level;
    this.pokemonHealth= 2*level;
    this.pokemonAttack=level;
    // methods
    this.tackle=function(targetPokemon){
        console.log(this.pokemonName+" used Tackle on "+targetPokemon.pokemonName);
        console.log(targetPokemon.pokemonName+" Hp is reduced by "+this.pokemonAttack);
        targetPokemon.pokemonHealth-=this.pokemonAttack;
        console.log("HP left is "+targetPokemon.pokemonHealth)
        if (targetPokemon===this){
            console.log("it's Super Effective!");
        }
        if (targetPokemon.pokemonHealth<=0){
            console.log(targetPokemon.pokemonName+" Has Fainted");
        }

    }
}
pikachu.tackle(geodude);
pikachu.tackle(geodude);
pikachu.tackle(geodude);

pikachu.tackle(pikachu);
pikachu.tackle(pikachu);

